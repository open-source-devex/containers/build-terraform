#!/usr/bin/env sh

set -v
set -e

if [ "${DEBUG_SCRIPT}" = "true" ]; then
    set -x
fi

CONTAINER_NAME="$1"
CONTAINER_TEST_IMAGE="$2"

docker rm -f ${CONTAINER_NAME} 2>&1 > /dev/null || true

docker run --name ${CONTAINER_NAME} -dt -e START_KEYBASE="false" ${CONTAINER_TEST_IMAGE}  ash

# tests
docker exec -t ${CONTAINER_NAME} ash -c 'type terraform'
docker exec -t ${CONTAINER_NAME} ash -c 'type ruby'
docker exec -t ${CONTAINER_NAME} ash -c 'type dog'
docker exec -t ${CONTAINER_NAME} ash -c 'inspec'

# clean up
docker rm -f ${CONTAINER_NAME}
